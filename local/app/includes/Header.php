<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Lancer Rental</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo URL ?>app/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <!-- Font Awesome core CSS -->
  <link href="<?php echo URL ?>app/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  
  <!-- Custom styles for this template -->
  <link href="<?php echo URL ?>app/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <img style="background-color:transparent;border:none;" class="img-fluid img-thumbnail" src="<?php echo URL ?>app/images/logo.png" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URL ?>public/home/">Home
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URL ?>public/home/about/">About Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URL ?>public/home/contact/">Contact</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URL ?>public/login/">Log In</a>
        </li>
    
      </ul>
    </div>
  </div>
</nav>

<div id="page-content" class="bg">
</div>
<!-- Page Content -->
<div id="page-content" class="container">