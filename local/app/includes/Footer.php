     </div>
     <!-- /. page container -->

     <!-- Footer -->
     <footer class="py-5 bg-dark" style=" position: fixed;
     bottom: 0;right:0;left:0">
       <div class="container">
        <div class="row vcenter">
          <div class="pull-left col-lg-4 col-xs-12">
            <p style="color:white">Copyright &copy;2017 by Lancer Rental Inc.</p>
          </div>
          <div class="col-lg-4 col-lg-offset-4" style="text-align: center">
            <a href="#"><i style="color:white;font-size:2em" class="fa fa-linkedin-square fa-icon"></i></a>
            <a href="#"><i style="color:white;font-size:2em" class="fa fa-twitter-square fa-icon"></i></a>
            <a href="#"><i style="color:white;font-size:2em" class="fa fa-facebook-square fa-icon"></i></a>
          </div>
          <div class="pull-left col-lg-4 col-xs-12">
            <a href="#"><p style="color:white;text-align: right">Terms of Service</p></a>
          </div>
        </div>
      </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo URL ?>app/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo URL ?>app/vendor/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>